\documentclass[10pt]{beamer}

% \usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{amsmath,amsthm}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{marvosym}
\usepackage{hyperref}
\usepackage{soul}
\usepackage{tikz}
\usepackage{wasysym}
%\usepackage{enumitem}

\usetikzlibrary{arrows,shapes.misc}

% setlist[itemize]{label={\footnotesize $\bullet$}, topsep=-0.2ex,
 %  partopsep=0pt, parsep=1pt, itemsep=0pt, leftmargin=3.5mm,
 %  labelsep=0.5ex} 

\newtheorem{thm}{Theorem}

\newcommand{\Rr}{\mathbb{R}}
\newcommand{\Nr}{\mathbb{N}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\R}{\mathcal{R}}
\newcommand{\Q}{\mathcal{Q}}
\renewcommand{\L}{\mathcal{L}}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\N}{\mathcal{N}}
\newcommand{\G}{\mathcal{G}}
\newcommand{\Hg}{\mathcal{H}}
\newcommand{\V}{\mathcal{V}}
\newcommand{\E}{\mathcal{E}}
\newcommand{\pa}{\text{Pa}}
\newcommand{\D}{\mathcal{D}}
\newcommand{\Oc}{\mathcal{O}}

\newcommand{\Hnull}{\mathcal{H}_{\text{null}}}
\newcommand{\Halt}{\mathcal{H}_{\text{alt}}}
\newcommand{\PT}[1]{$\text{P}_\text{#1}$}

%\newcommand{\strong}[1]{{\color{red}#1}}
\newcommand{\balph}{\boldsymbol{\alpha}}
\DeclareMathOperator*{\argmax}{argmax}



\definecolor{frias}{rgb}{0,0.616,0.580}%{0,0.627,0.627}%{0,0.729,0.71}

\definecolor{uibr}{rgb}{0.811,0.235,0.227}
\definecolor{uibg}{rgb}{0.541,0.671,0.404}

\newcommand{\strong}[1]{{\color{uibr}#1}}

\newcommand{\chm}{{\large \strong{\smiley}}}
\newcommand{\crs}{{\large\color{red}\frownie}}

\usetheme{Malmoe}
\usecolortheme[named=uibr]{structure}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{}
\setbeamertemplate{enumerate subitem}{{\normalsize(\alph{enumii})}}

\graphicspath{{./}{../../Logos/}{../../Figures/}{../../2012/CRI-2012/}}

\title{\textcolor{black}{Machine learning in systems biology}}

\subtitle{\textcolor{black}{\\{Bergen AI \& Machine Learning Symposium}}}

\author{Tom Michoel}

\date{25 March 2019}

\begin{document}

\begin{frame}
  \thispagestyle{empty}

  \vspace*{-7mm}
  \hspace*{-10mm}\includegraphics[width=\paperwidth]{uib-header}
  

  \titlepage

  %\vspace*{-3mm}
  
\end{frame}

\begin{frame}
  \frametitle{The challenge of {\footnotesize (one aspect of)}
      systems biology}
    \fontsize{9}{11}\selectfont
  \begin{columns}
    \begin{column}{.3\linewidth}
      \begin{center}
        \includegraphics[width=.8\linewidth]{healthy_heart_1}

        $\vdots$
        
        \includegraphics[width=.8\linewidth]{healthy_heart_2}

        \fontsize{5}{6}\selectfont
        {\tiny \url{https://www.nhs.uk/conditions/nhs-health-check/check-your-heart-age-tool/}}
      \end{center}
    \end{column}
    \begin{column}{.05\linewidth}
      \fontsize{25}{30}\selectfont
      \begin{align*}
        \Rightarrow
      \end{align*}
    \end{column}
    \begin{column}{0.4\linewidth}
      \begin{minipage}[c]{\linewidth}
        \includegraphics[width=\linewidth]{figNet}
      \end{minipage}
      \begin{center}
        {\tiny [Schadt. Nature (2009)]}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \fontsize{9}{11}\selectfont
  \begin{columns}
    \begin{column}{0.42\linewidth}
      \begin{minipage}[c]{\linewidth}
        \includegraphics[width=\linewidth]{figNet}
      \end{minipage}
    \end{column}
    \begin{column}{0.62\linewidth}
      \textbf{Yes, we can: measure ``omics''}
      \begin{itemize}
      \item $\Oc(10^6)$ genetic variants, 
      \item $\Oc(10^5)$ epigenetic markers, 
      \item $\Oc(10^4)$ genes, proteins and metabolites,
      \item In multiple cell types, tissues and organs in an individual, 
      \item At single cell resolution if need be.
      \end{itemize}

      \medskip

      \textbf{No, we can't:}
      \begin{itemize}
      \item Learn reproducible and predictive models,
      \item Do much better than a `simple' heart-age tool.
      \end{itemize}

      \medskip

      \textbf{Why not?}
      \begin{itemize}
      \item Noise, biases, confounders {\tiny (it's not rocket
          science, alas)}
      \item Correlation $\neq$ causation,
      \item $p\gg n$,
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}
  \frametitle{Noise, biases, confounders: latent factor models}
  \fontsize{9}{11}\selectfont

  \begin{center}
    \includegraphics[width=.95\linewidth]{stegle2010_fig1}\\
    {\tiny [Stegle \emph{et al}. PLOS Comp Biol (2010)]}
  \end{center}
  \vspace*{-3mm}
  \begin{columns}
    \begin{column}[t]{.5\linewidth}
      \begin{itemize}
      \item Correct data matrices for \textbf{known} and
        \textbf{unknown} confounding factors.
      \item Model systematic effects on mean and covariance structure
        of samples with \textbf{mixed models}.
      \end{itemize}  
    \end{column}
    \begin{column}[t]{.5\linewidth}
      \begin{itemize}
      \item Gaussian models relate to probabilistic PCA.
      \item Non-Gaussian models account for heavy-tail effect sizes.
      \end{itemize}
    \end{column}
  \end{columns}
  
\end{frame}

\begin{frame}
  \frametitle{Correlation $\neq$ causation: causal inference}
  %\fontsize{9}{11}\selectfont
  \begin{center}
    \textbf{In biology, genetic variation causally precedes \textit{everything}.}\\[4mm]

    \begin{minipage}{.5\linewidth}
      \begin{center}
        \includegraphics[width=\linewidth]{hemani-2017-fig1}

        {\tiny [Hemani \emph{et al}.  PLOS Genet (2017)]}
      \end{center}
    \end{minipage}\hspace*{10mm}
    \begin{minipage}{.3\linewidth}
      \begin{center}
        \begin{tikzpicture}[baseline=-5mm,scale=1.]
          \fontsize{8}{11}\selectfont
          \node (X) at (1,0) {$X$};
          \node (Y) at (2.4,0) {$Y$};
          \node (Ex) at (0.,0.) {$E_X$};
          \node (Ez) at (0.,-1) {$E_Z$};
          \node (Z) at (1.,-1) {$Z$};
          \path[->,thick] (Ex) edge (X);
          \path[->,thick] (X) edge node[above] {$a$} (Y);
          \path[<->,dashed,thick] (X) edge [out=60,in=120]  (Y);
          \path[<->,dashed,thick] (Z) edge [out=-10,in=-100] (Y);
          \path[<->,thick,dashed] (Ex) edge [out=210,in=150] (Ez);
          \path[->,thick] (Ez) edge (Z);
          \path[->,thick] (Z) edge (Y);
          \path[->,thick] (Z) edge (X);
          \path[<->,thick,dashed] (X) edge [out=210,in=150] (Z);
        \end{tikzpicture}
      \end{center}
    \end{minipage}

    % \includegraphics[width=.7\linewidth]{nrg2612-f4}\\
    % {\tiny [Mackay, Nat Rev Genet (2009)]}
  \end{center}
  \fontsize{9}{11}\selectfont
  \vspace*{-3mm}
  \begin{columns}
    \begin{column}[t]{.5\linewidth}
      \begin{itemize}
      \item Instrumental variable methods.
      \item Fast software for comparing millions of variable pairs
        with FDR correction.
      \end{itemize}
    \end{column}
    \begin{column}[t]{.5\linewidth}
      \begin{itemize}
      \item Path analysis methods to model ``pleiotropic'' effects.
      \item Methods using summary statistics alone to avoid accessing
        privacy-sensitive genetic data.
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{$p\gg n$: data fusion and high-dimensional Bayesian
    network inference}
  \vspace*{-5mm}
  \begin{center}
    \includegraphics[width=.75\linewidth]{fig-method}
  \end{center}
  \fontsize{9}{11}\selectfont
  \vspace*{-3mm}
  \begin{columns}
    \begin{column}[t]{.5\linewidth}
      \begin{itemize}
      \item \textbf{Greedy causal BN learning}
        \begin{enumerate}
        \item Pairwise causal inference.
        \item Topological ordering.
        \item Independent variable selections.
        \end{enumerate}
      \end{itemize}
    \end{column}
    \begin{column}[t]{.5\linewidth}
      \begin{itemize}
      \item \textbf{Data fusion}
        \begin{itemize}
        \item Observational data in humans.
        \item Interventional and time course data in model systems.
        \end{itemize}
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

% \begin{frame}
%   \frametitle{Acknowledgements}
  
%   \begin{columns}
%     \begin{column}[t]{.33\linewidth}
%       \begin{block}{Group members}
%         \textbf{Sean Bankier}\\
%         \textbf{Pau Erola}\\
%         \textbf{Siddharth Jayaraman}\\
%        \textbf{Lingfei Wang}\\     
%       \end{block}
%       \begin{block}{Edinburgh}
%         Ruth Andrew\\
%         Brian Walker\\
%       \end{block}
%     \end{column}
%     \begin{column}[t]{.33\linewidth}
%       \begin{block}{Ghent University}
%         Pieter Audenaert
%       \end{block}
%       \begin{block}{ISMMS/Karolinska}
%         Ariella Cohain\\        
%         \textbf{Hassan Foroughi Asl}\\
%         Oscar Franz\'en\\
%         \textbf{Husain Talukdar}\\
%         Eric Schadt\\
%         Johan Bj\"orkegren\\
%       \end{block}
%     \end{column}
%     \begin{column}[t]{.34\linewidth}
%       \vspace*{-10mm}

%       \hspace*{16mm}\begin{minipage}{.6\linewidth}
%         \includegraphics[width=\linewidth]{UiB-emblem_gray}
%       \end{minipage}

%       \begin{flushright}
%         \includegraphics[width=.35\linewidth]{British_Heart_Foundation}\\[5mm]

%         \includegraphics[width=\linewidth]{bbsrc-colour}\\[5mm]

%         \hspace*{-21mm}\includegraphics[height=.9cm]{NIH}
%       \end{flushright}
%     \end{column}
%   \end{columns}
%   \vspace*{-14mm}
%   \hspace*{-4mm}\begin{minipage}{.3\linewidth}
%     \includegraphics[width=\linewidth]{Cbu_logo_black_transparent}
%   \end{minipage}
  
%   \vspace*{2mm}

%   \begin{center}
%     \textbf{{\large http://lab.michoel.info}}
%   \end{center}
% \end{frame}

\begin{frame}
  \begin{center}
    \begin{minipage}{.28\linewidth}
      \includegraphics[width=\linewidth]{Cbu_logo_black_transparent}
    \end{minipage}%\hfill
    \begin{minipage}{.7\linewidth}
      \begin{center}
        \strong{\url{http://lab.michoel.info}}
        
        \medskip
        
        \strong{\url{https://www.cbu.uib.no}}
      \end{center}
    \end{minipage}%
    % \begin{minipage}{.2\linewidth}
    %   \includegraphics[width=\linewidth]{UiB-emblem_gray}
    % \end{minipage}
  \end{center}

  \vspace*{10mm}

  \begin{quote}
     All the impressive achievements of deep learning amount to just
     curve fitting. To build truly intelligent machines, teach them
     cause and effect.
  \end{quote}
  \begin{flushright}
    Judea Pearl (2018)
  \end{flushright}

  

\end{frame}


\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
